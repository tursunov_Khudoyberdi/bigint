package bigint

import (
	"errors"
	"strconv"
)

type Bigint struct {
	Value string
}

func NewInt(num string) (Bigint, error) {
	if _, err := strconv.ParseUint(num, 10, 64); err != nil {
		return Bigint{}, errors.New("invalid input")
	}

	return Bigint{
		Value: num,
	}, nil
}

func (z *Bigint) Set(num string) error {
	if _, err := strconv.ParseUint(num, 10, 64); err != nil {
		return errors.New("invalid input")
	}

	z.Value = num
	return nil
}

func Add(a, b Bigint) Bigint {
	c, _ := strconv.ParseUint(a.Value, 10, 64)
	d, _ := strconv.ParseUint(b.Value, 10, 64)
	sum := c + d

	return Bigint{strconv.FormatUint(uint64(sum), 10)}
}

func Sub(a, b Bigint) Bigint {
	e, _ := strconv.ParseUint(a.Value, 10, 64)
	f, _ := strconv.ParseUint(b.Value, 10, 64)
	difference := e - f

	return Bigint{strconv.FormatUint(uint64(difference), 10)}
}

func Multiply(a, b Bigint) Bigint {
	g, _ := strconv.ParseUint(a.Value, 10, 64)
	h, _ := strconv.ParseUint(b.Value, 10, 64)
	multiplication := g * h

	return Bigint{strconv.FormatUint(uint64(multiplication), 10)}
}

func Mod(a, b Bigint) Bigint {
	//
	// MATH Mod
	//
	return Bigint{Value: ""}
}
